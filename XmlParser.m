/*
 
 *  Created by Cyber Infrastructure (p) Ltd. on 26/05/09.
 *  Copyright Cyber Infrastructure (p) Ltd. 2004-2008. 
 *  All rights reserved.
 
 */

#import "XmlParser.h"


@implementation XmlParser

@synthesize result;
@synthesize currentElementName;
@synthesize currentElementValue;
@synthesize parentArray;


- (void)parserDidStartDocument:(NSXMLParser *)parser
{
	result=[[NSMutableDictionary alloc] init];
	parentArray=[[NSMutableArray alloc] init];
	[parentArray addObject:result];
	currentElementName=@"";
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if (qName) {
		elementName = qName;
	}
	currentElementValue=@"";
	if (currentElementName!=@"")
	{
		id newParent=NULL;
		if ([currentElementName isLike:@"*Array*"])
		{
			newParent=[[NSMutableArray alloc] init];
		} else {
			newParent=[[NSMutableDictionary alloc] init];
		}
		[parentArray addObject:newParent];
	}
	currentElementName=elementName;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{     
	if (qName) {
		elementName = qName;
	}
	
	if (currentElementName==@"")
	{
		//We're adding a container with children.  Add it to the parent and remove this item fromt he parentArray
		int currentIndex=[parentArray count]-1;
		int parentIndex=currentIndex - 1;
		id currentChild=[parentArray objectAtIndex:currentIndex];
		id currentParent=[parentArray objectAtIndex:parentIndex];
		
		if ([currentParent isKindOfClass:[NSMutableArray class]])
		{
			[currentParent addObject:currentChild];
		} else {
			[currentParent setObject:currentChild forKey:elementName];
		}
		
		[parentArray removeObjectAtIndex:currentIndex];
	} else {
		//We're adding a simple type element
		int currentIndex=[parentArray count]-1;
		id currentParent=[parentArray objectAtIndex:currentIndex];
		if ([currentParent isKindOfClass:[NSMutableArray class]])
		{
			[currentParent addObject:currentElementValue];
		} else {
			[currentParent setObject:currentElementName forKey:currentElementName];
		}
	}
	currentElementName=@"";
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	currentElementValue=string;
}

@end