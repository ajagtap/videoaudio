//
//  ViewController.m
//  DemoAppWithCoreData
//
//  Created by CIS Mac Mini 8 on 07/03/15.
//  Copyright (c) 2015 Cyber Infrastructure Pvt. Lmt. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()<UITextFieldDelegate>{
    // Text Filed declaration
    IBOutlet UIScrollView *scrollViewConatainer;
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtGender;
    IBOutlet UITextField *txtCountry;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtConfirmPassword;

    IBOutlet UIButton *btnRegister;

}
@end

@implementation SignUpViewController



- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewWillAppear:(BOOL)animated{
  
}

- (void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [scrollViewConatainer setContentOffset:CGPointMake(0, btnRegister.frame.origin.y + btnRegister.frame.size.height) animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark Validation

#pragma mark-
#pragma mark Action

- (IBAction)btnPressedLogin:(id)sender{
    // Login Button Pressed event, apply validation & perform database action.
}


#pragma mark-
#pragma mark Dissmiss keyBoard

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [txtConfirmPassword resignFirstResponder];
    [txtCountry resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtGender resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtUserName resignFirstResponder];
    [scrollViewConatainer setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark-
#pragma mark TextFiledDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self adjustTextFiled:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (void)adjustTextFiled:(UITextField *)textfileld {
    if (textfileld.center.y > scrollViewConatainer.frame.size.height/3 ) {
        [scrollViewConatainer setContentOffset:CGPointMake(0, textfileld.center.y - scrollViewConatainer.frame.size.height/2 + 80) animated:YES];
    }else{
        [scrollViewConatainer setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

@end
