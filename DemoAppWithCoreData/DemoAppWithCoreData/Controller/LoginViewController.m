//
//  ViewController.m
//  DemoAppWithCoreData
//
//  Created by CIS Mac Mini 8 on 07/03/15.
//  Copyright (c) 2015 Cyber Infrastructure Pvt. Lmt. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController (){
    // Text Filed declaration
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtPassword;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark Validation

#pragma mark-
#pragma mark Action

- (IBAction)btnPressedLogin:(id)sender{
    // Login Button Pressed event, apply validation & perform database action.
}


@end
