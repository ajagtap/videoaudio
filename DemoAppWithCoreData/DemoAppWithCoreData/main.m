//
//  main.m
//  DemoAppWithCoreData
//
//  Created by CIS Mac Mini 8 on 07/03/15.
//  Copyright (c) 2015 Cyber Infrastructure Pvt. Lmt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
